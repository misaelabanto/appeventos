const express = require('express');
const path = require('path');
const Database = require('./db');

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Renderización
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(express.static(path.join(__dirname, 'public')));

/** Backend (REST API) **/ 

// Listar eventos
app.get('/evento/listar', (req, res) => {
	res.json(Database.obtenerTodos());
});
// Crear evento
app.post('/evento/crear', (req, res) => {
	Database.nuevoEvento(req.body);
	res.json({error: false, mensaje: 'Evento creado con éxito'});
});
// Modificar evento
app.post('/evento/modificar', (req, res) => {
	let {idEvento, ...evento} = req.body;
	Database.modificar(idEvento, evento);
	res.json({error: false, mensaje: 'Evento modificado con éxito'});
});
// Eliminar evento
app.post('/evento/eliminar', (req, res) => {

});

/** Frontend **/
// Index
app.get('/', (req, res) => {
	res.render('index', {title: 'Aplicación de Eventos'});
});
// Administrar eventos
app.get('/admin', (req, res) => {
	res.render('admin', {title: 'Crear evento'});
});

app.get('/admin/editar', (req, res) => {
	let idEvento = req.query.idEvento;
	res.render('editarEvento', {title: 'Editar Evento', evento: Database.obtenerPorId(idEvento)});
});

app.listen(process.env.PORT || 3000, () => {
	console.log('Server running on port 3000');
})