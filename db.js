const fs = require('fs');

let eventos = JSON.parse(fs.readFileSync('eventos.json'));

function guardarArchivo() {
	fs.writeFileSync('eventos.json', JSON.stringify(eventos, null, 2));
}

class Database {
	static obtenerTodos() {
		return eventos;
	}

	static obtenerPorId(idEvento) {
		return eventos[idEvento];
	}

	static nuevoEvento(nuevoEvento) {
		let claves = Object.keys(eventos);
		let ultimoId = Number(claves[claves.length - 1]);
		eventos[ultimoId + 1] = nuevoEvento;
		guardarArchivo();
	}

	static modificar(idEvento, nuevoEvento) {
		eventos[idEvento] = nuevoEvento;
		guardarArchivo();
	}

	static eliminar(idEvento) {
		delete eventos[idEvento];
		guardarArchivo();
	}
}

module.exports = Database;