function crearEvento() {
	let nuevoEvento = {
		nombre: $('#inputNombre').val(),
		descripcion: $('#inputDescripcion').val(),
		fecha: $('#inputFecha').val(),
		hora: $('#inputHora').val(),
		lugar: $('#inputLugar').val(),
		imagenUrl: $('#inputUrlImagen').val(),
		expositores: $('#inputExpositores').val().split('\n')
	};

	// validaciones
	if(nuevoEvento.nombre.length === '') return alert('Debe indicar un nombre del evento');

	fetch('/evento/crear', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify(nuevoEvento)
	}).then(res => {
		if(res.ok) return res.json();
		else throw Error(res.statusText);
	}).then(resultado => {
		alert(resultado.mensaje);
	}).catch(reason => {
		alert(reason);
	});
}

function modificarEvento() {
	let idEvento = location.href.split('?')[1].split('=')[1];
	console.log({idEvento});
	let eventoModificado = {
		idEvento,
		nombre: $('#inputNombre').val(),
		descripcion: $('#inputDescripcion').val(),
		fecha: $('#inputFecha').val(),
		hora: $('#inputHora').val(),
		lugar: $('#inputLugar').val(),
		imagenUrl: $('#inputUrlImagen').val(),
		expositores: $('#inputExpositores').val().split('\n')
	};
	// validaciones
	if(eventoModificado.nombre.length === '') return alert('Debe indicar un nombre del evento');

	fetch('/evento/modificar', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify(eventoModificado)
	}).then(res => {
		if(res.ok) return res.json();
		else throw Error(res.statusText);
	}).then(resultado => {
		alert(resultado.mensaje);
	}).catch(reason => {
		alert(reason);
	});
}