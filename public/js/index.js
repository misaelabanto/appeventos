function getEventos() {
	fetch('/evento/listar')
		.then(res => {
			return res.json();
		})
		.then(eventos => {
			poblarEventos(eventos);
		});
}

function poblarEventos(eventos) {
	for (let idEvento in eventos) {
		let evento = eventos[idEvento];
		$('#bloqueEventos').append(`
		<div class="card" style="width: 18rem;">
			<img src="${evento.imagenUrl}" class="card-img-top" alt="...">
			<div class="card-body">
				<h5 class="card-title">${evento.nombre}</h5>
				<p class="card-text">${evento.descripcion}</p>
				<h5 class="text-warning">Lugar: ${evento.lugar}</h5>
			</div>
			<ul id="expositores-${idEvento}" class="list-group list-group-flush">
				
			</ul>
			<div class="card-body">
				<a href="#" class="card-link">${evento.fecha}</a>
				<a href="#" class="card-link">${evento.hora}</a>
			</div>
		</div>
		`);
		evento.expositores.forEach(expositor => {
			$('#expositores-' + idEvento).append(`
			<li class="list-group-item">${expositor}</li>
			`);
		});
	}
}

$(function() {
	getEventos();
});